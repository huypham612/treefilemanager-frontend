# README #

* Open the TreeFileManager.sln file via Visual Studio, and run it on Google Chrome or any other browsers.

### What is this repository for? ###

* An opened-source prototype of a File Management system that uses a treelike structure.
* 1.0
* Note that there is no call to the databases in this version due to my Parse account's privacy. Therefore, the front end is using a fake JSON object as a prototype.

### How do I get set up? ###

* Install and open the project via Visual Studio. 
* No other dependencies nor configurations needed.

### Contribution guidelines ###

* Feel free to download and do something cool with the code.

###Frameworks that I used ###

* https://angularjs.org/
* https://github.com/angular-ui-tree/angular-ui-tree
* http://angular-ui.github.io/bootstrap/
* http://getbootstrap.com/
* http://www.asp.net/