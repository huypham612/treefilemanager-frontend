﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Ajax.Utilities;
using TreeFileManager.Models;

namespace TreeFileManager.Controllers.api
{
    public class CrudController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<Node> Get()
        {
            return createTree();
        }

        // GET api/<controller>/5
        public Node Get(int id)
        {
            throw new NotImplementedException();
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        private IEnumerable<Node> createTree()
        {
            var leaf1 = new Node()
            {
                id = 2,
                title = "Leaf 1",
                iconCls = "glyphicon glyphicon-file",
                isLeaf = true,
                deployed = "01/01/2011 5:05:05 PM as a test",
                description = "test",                
                createdDate = "01/01/2011 01:01:30 PM",
                modifiedDate = "01/01/2011 01:01:30 PM"
            };

            var leaf2 = new Node()
            {
                id = 3,
                title = "Leaf 2",
                iconCls = "glyphicon glyphicon-file",
                isLeaf = true,
                deployed = "02/02/2022 5:05:05 PM as a test 2",
                description = "test",
                createdDate = "02/01/2011 01:01:30 PM",
                modifiedDate = "02/01/2011 05:01:30 PM"
            };

            var parent2 = new Node()
            {
                id = 4,
                title = "Parent 2",
                collapsed = false,
                hideChildren = false,
                iconCls = "glyphicon glyphicon-folder-close",
                children = new List<Node>() { leaf1, leaf2 },
                description = "test",
                createdDate = "02/01/2011 01:01:30 PM",
                modifiedDate = "02/01/2011 05:01:30 PM"
            };

            var parent1 = new Node()
            {
                id = 1,
                title = "Parent 1",
                collapsed = false,
                hideChildren = false,
                iconCls = "glyphicon glyphicon-folder-close",
                children = new List<Node>() { parent2 },
                description = "test",
                createdDate = "03/01/2011 01:01:30 PM",
                modifiedDate = "03/01/2011 05:01:30 PM"
            };

            var parent3 = new Node()
            {
                id = 5,
                title = "Parent 3",
                collapsed = false,
                hideChildren = false,
                iconCls = "glyphicon glyphicon-folder-close",
                children = new List<Node>(),
                description = "test",
                createdDate = "04/01/2011 01:01:30 PM",
                modifiedDate = "04/01/2011 05:01:30 PM"
            };

            return new List<Node>() { parent1, parent3 };
        }
    }
}