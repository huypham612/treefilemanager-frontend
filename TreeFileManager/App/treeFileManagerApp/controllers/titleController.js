﻿treeFileManagerApp.controller('modalCreditsController', function ($scope, $modalInstance, creditsData) {
    $scope.creditsData = creditsData;

    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };
}).controller('modalFeaturesController', function ($scope, $modalInstance, featuresData) {
    $scope.featuresData = featuresData;

    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };
}).controller('titleController', function ($scope, $modal) {
    $scope.data = {
        "AngularJS": "https://angularjs.org/",
        "Angular-UI-Tree": "https://github.com/angular-ui-tree/angular-ui-tree",
        "AngularUI": "http://angular-ui.github.io/bootstrap/",
        "Bootstrap": "http://getbootstrap.com/",
        "ASP.NET": "http://www.asp.net/"
    };

    $scope.bitBucketLink = "https://huypham612@bitbucket.org/huypham612/treefilemanager-frontend.git";

    $scope.openCredits = function () {
        var modalInstance = $modal.open({
            templateUrl: 'App/treeFileManagerApp/partials/modalCredits.html',
            controller: 'modalCreditsController',
            size: '',
            resolve: {
                creditsData: function () {
                    return $scope.data;
                }
            }
        });
    };

    $scope.featuresData = [
        "Nested folder structure", "Drag & drop organizing", "Sort by names & date", "Search by file names", "..."
    ];

    $scope.openFeatures = function () {
        var modalInstance = $modal.open({
            templateUrl: 'App/treeFileManagerApp/partials/modalFeatures.html',
            controller: 'modalFeaturesController',
            size: '',
            resolve: {
                featuresData: function () {
                    return $scope.featuresData;
                }
            }
        });
    };
});