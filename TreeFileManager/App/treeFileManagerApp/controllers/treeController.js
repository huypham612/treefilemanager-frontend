﻿treeFileManagerApp.controller('treeController', function ($scope, $filter, $modal, crudService) {
    $scope.trees = undefined;
    getTree();
    $scope.currentNode = {};
    $scope.dateFormat = 'MM/dd/yyyy hh:mm:ss a';
    $scope.modal = {
        title: '',
        isFolder: false,
        inputData: { title: '', description: '' }
    };

    $scope.modalInfoData = {};

    $scope.search = {
        data: [],
        pushNode: function (node) {
            for (var i = 0; i < this.data.length; i++) {
                var item = this.data[i];
                if (item.title == node.title) {
                    return;
                }
            }

            this.data.push(node);
        }
    };

    $scope.sort = {
        orderBy: $filter('orderBy'),
        byDateAsc: false,
        byNameAsc: false,
        toggleSortNameNode: function (node) {
            var sortingOrder;
            node.byNameAsc = !node.byNameAsc;
            sortingOrder = node.byNameAsc ? '+title' : '-title';
            node.children = this.orderBy(node.children, sortingOrder);
        },
        toggleSortDateNode: function (node) {
            var sortingOrder;
            node.byDateAsc = !node.byDateAsc;
            sortingOrder = node.byDateAsc ? '+modifiedDate' : '-modifiedDate';
            node.children = this.orderBy(node.children, sortingOrder);
        },
        toggleSortName: function () {
            var sortingOrder;
            this.byNameAsc = !this.byNameAsc;
            sortingOrder = this.byNameAsc ? '+title' : '-title';
            $scope.trees = this.orderBy($scope.trees, sortingOrder);
        },
        toggleSortDate: function () {
            var sortingOrder;
            this.byDateAsc = !this.byDateAsc;
            sortingOrder = this.byDateAsc ? '+modifiedDate' : '-modifiedDate';
            $scope.trees = this.orderBy($scope.trees, sortingOrder);
        }
    };

    $scope.open = function (currentNode, isFolder) {
        var modalInstance = $modal.open({
            templateUrl: 'App/treeFileManagerApp/partials/modal.html',
            controller: 'modalController',
            size: '',
            resolve: {
                modal: function () {
                    $scope.modal.title = isFolder ? "Add a new Folder" : "Add a new file";
                    return $scope.modal;
                }
            }
        });

        modalInstance.result.then(function (modal) {
            var id = Math.floor((Math.random() * 100000) + 10);
            var iconCls = modal.isFolder ? "glyphicon glyphicon-folder-close" : "glyphicon glyphicon-file";
            var children = isFolder ? [] : null;
            var date = $filter('date')(new Date, $scope.dateFormat);
            var newNode =
            {
                id: id,
                title: modal.inputData.title,
                description: modal.inputData.description,
                iconCls: iconCls,
                isLeaf: !isFolder,
                collapsed: false,
                hideChildren: false,
                children: children,
                deployed: '',
                createdDate: date,
                modifiedDate: date,
                sortingOrder: "-modifiedDate"
            };
            addNewNode();
            $scope.currentNode = newNode;

            function addNewNode() {
                if (currentNode == null) {
                    $scope.trees.push(newNode);
                } else {
                    currentNode.collapsed = false;
                    currentNode.hideChildren = false;
                    currentNode.children.push(newNode);
                }
            }
        }, function () {
        });
    };

    $scope.openModalInfo = function (node) {
        var modalInstance = $modal.open({
            templateUrl: 'App/treeFileManagerApp/partials/modalInfo.html',
            controller: 'modalInfoController',
            size: '',
            resolve: {
                modalInfoData: function () {
                    var displayInfo =
                        {
                            'Id': node.id,
                            'Title': node.title,
                            'Description': node.description,
                            'Deployed': node.deployed,
                            'Date Created': node.createdDate,
                            'Date Modified': node.modifiedDate
                        };

                    $scope.modalInfoData.node = displayInfo;
                    return $scope.modalInfoData;
                }
            }
        });
    };

    function getTree() {
        var promiseGet = crudService.getAll();

        promiseGet.then(
            function (response) {
                $scope.trees = response.data;
            },
            function (errorPl) {
                console.log('failure gettting tree', errorPl);
            });
    }

    $scope.editFile = function (node) {
        var modalInstance = $modal.open({
            templateUrl: 'App/treeFileManagerApp/partials/modal.html',
            controller: 'modalController',
            size: '',
            resolve: {
                modal: function () {
                    $scope.modal.title = "Edit";
                    $scope.modal.inputData.title = node.title;
                    $scope.modal.inputData.description = node.description;
                    return $scope.modal;
                }
            }
        });

        modalInstance.result.then(function (modal) {
            node.title = modal.inputData.title;
            node.description = modal.inputData.description;
            node.modifiedDate = $filter('date')(new Date, $scope.dateFormat);
            $scope.currentNode = node;
        }, function () {
        });
    };

    $scope.treeOptions = {
        dropped: function (event) {
            var nodesScope = event.dest.nodesScope;
            if (nodesScope.$nodeScope == null) return;
            var destNode = nodesScope.$nodeScope.$modelValue;
            destNode.collapsed = false;
            destNode.hideChildren = false;
            $scope.currentNode = destNode;
        },
    };

    $scope.deleteNode = function (scope) {
        $scope.currentNode = scope.$parent.$nodeScope.$modelValue;
        scope.remove();
    };

    $scope.toggleNode = function (node) {
        $scope.currentNode = node;

        node.collapsed = !node.collapsed;
        $scope.updateLeaves(node, node.collapsed, false);
    };

    $scope.updateLeaves = function (node, collapsed, isForced) {
        if (!node.isLeaf) {
            node.collapsed = isForced ? collapsed : node.collapsed;
            node.hideChildren = collapsed ? collapsed : node.collapsed;
            angular.forEach(node.children, function (child) {
                $scope.updateLeaves(child, collapsed, isForced);
            });
        }
    }
});