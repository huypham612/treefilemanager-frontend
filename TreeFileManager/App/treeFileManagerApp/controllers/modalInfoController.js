﻿treeFileManagerApp.controller('modalInfoController', function ($scope, $modalInstance, modalInfoData) {
    $scope.modalInfoData = modalInfoData;

    $scope.closeModalInfo = function () {
        $modalInstance.close();
    };
})