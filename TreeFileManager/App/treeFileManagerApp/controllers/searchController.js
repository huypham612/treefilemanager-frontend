﻿treeFileManagerApp.controller('searchController', function ($scope, $filter, $modal) {
    $scope.search.selected = undefined;
    $scope.search.placeHolder = 'File Name';
    $scope.search.onSelect = function (item, model, label) {
        $scope.openModalInfo(item);
        $scope.currentNode = item;
    };

    $scope.hideAll = function (collapseAll) {
        angular.forEach($scope.trees, function (child) {
            child.collapsed = collapseAll;
            $scope.updateLeaves(child, collapseAll, true);
        });
    };
})