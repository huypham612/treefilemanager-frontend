﻿treeFileManagerApp.controller('modalController', function ($scope, $modalInstance, modal) {
    $scope.modal = modal;    

    $scope.submitModal = function () {
        $modalInstance.close($scope.modal);
    };

    $scope.cancelModal = function () {
        $modalInstance.dismiss('cancel');
    };
})