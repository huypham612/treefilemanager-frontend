﻿treeFileManagerApp.service('crudService', function ($http) {
    var baseUrl = "/api/";
    var apiController = "Crud";

    this.post = function (data) {
        var request = $http({
            method: "post",
            url: baseUrl + apiController,
            data: data
        });
        return request;
    }

    this.get = function (id) {
        return $http.get(baseUrl + apiController + "/" + id);
    }

    this.getAll = function () {
        return $http.get(baseUrl + apiController);
    }

    this.put = function (id, data) {
        var request = $http({
            method: "put",
            url: baseUrl + apiController + "/" + id,
            data: data
        });
        return request;
    }

    this.delete = function (data) {
        var request = $http({
            method: "delete",
            url: baseUrl + apiController + "/" + data
        });
        return request;
    }
});