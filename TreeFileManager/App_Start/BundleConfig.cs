﻿using System.Web;
using System.Web.Optimization;

namespace TreeFileManager
{
    /// <summary>
    /// Not using this.
    /// </summary>
    public class BundleConfig
    {        
        public static void RegisterBundles(BundleCollection bundles)
        {            

            bundles.Add(new ScriptBundle("~/bundles/treeFileManagerApp").Include(
                "~/App/treeFileManagerApp/*.js",
                "~/App/treeFileManagerApp/controllers/*.js",
                "~/App/treeFileManagerApp/services/*.js"));         

            bundles.Add(new ScriptBundle("~/bundles/uiBootstrap").Include(
                        "~/Scripts/bootstrap-gh-pages/ui-bootstrap-0.12.1.js",
                        "~/Scripts/bootstrap-gh-pages/ui-bootstrap-tpls-0.12.1.js",
                        "~/Scripts/bootstrap-gh-pages/ui-bootstrap-0.12.1.min.js",
                        "~/Scripts/bootstrap-gh-pages/ui-bootstrap-tpls-0.12.1.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapjs").IncludeDirectory(
                        "~/Content/themes/bootstrap-3.3.2-dist/js/", "*.min.js"));

            bundles.Add(new StyleBundle("~/Content/themes/angular-ui/css").Include(
                        "~/Content/themes/angular-ui/css/*.css"));           
            
            bundles.Add(new StyleBundle("~/Content/themes/bootstrap-3.3.2-dist/css").Include(
                "~/Content/themes/bootstrap-3.3.2-dist/css/*.css",
                "~/Content/themes/bootstrap-3.3.2-dist/css/*.min.css"));            
        }
    }
}