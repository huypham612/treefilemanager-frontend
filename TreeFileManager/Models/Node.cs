﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TreeFileManager.Models
{
    public class Node
    {
        [Required]
        public int id { get; set; }
        public string title { get; set; }
        public string iconCls { get; set; }        
        public bool isLeaf { get; set; }
        public bool collapsed { get; set; }
        public bool hideChildren { get; set; }
        public List<Node> children { get; set; }
        public string description { get; set; }
        public string createdDate { get; set; }
        public string modifiedDate { get; set; }
        public string deployed { get; set; }        
    }
}